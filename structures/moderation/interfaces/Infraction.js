class Infraction {

    constructor(client, info) {

        Object.defineProperty('this', client, { value: client });

        //General
        this.type = info.type;
        this.moderator = info.moderator;
        this.infractee = info.infractee; //the fuck?
        this.reason = info.reason || 'N/A';
        this.timestamp = info.timestamp;
        this.guild = info.guild;

        //Functional
        this.duration = info.duration || 0; //duration in seconds, 0 if none

    }

    get infractionId() {

    }

}

module.exports = Infraction;
