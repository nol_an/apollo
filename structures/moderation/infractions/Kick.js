const Infraction = require('./interfaces/Infraction.js');

class Kick extends Infraction {

    constructor(client, info) {

        super(client, {
            type: 'kick'
        });

        Object.defineProperty('this', client, { value: client });

    }

}

module.exports = Kick;
