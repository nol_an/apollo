const Resolver = require('../interfaces/Resolver.js');

class UserResolver extends Resolver {

    constructor(client) {

        super(client, {
            name: 'userResolver'
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    run(str, guild = false) {

        let user;

        if(/<@!?(\d{15,20})>/iy.test(str)) {
            const matches = /<@!?(\d{15,20})>/iy.exec(str);
            user = guild ? guild.members.get(matches[1]) : this.client.users.get(matches[1]);
            if(!user) return false;
        } else if (/\d{15,20}/iy.test(str)) {
            const matches = /(\d{15,20})/iy.exec(str);
            user = guild ? guild.members.get(matches[1]) : this.client.users.get(matches[1]);
            if(!user) return false;
        } else if(/(.{2,32})#(\d{4})/iy.test(str)) {
            const matches = /(.{2,32})#(\d{4})/iy.exec(str);
            user = guild
                ? guild.members.fiterArray(m=>m.user.username === matches[1] && m.user.discriminator === matches[2])[0]
                : this.client.users.filterArray(u=>u.username === matches[1] && u.discriminator === matches[2])[0];
            if(!user) return false;
        }

        return user;

    }

}

module.exports = UserResolver;
