const Resolver = require('../interfaces/Resolver.js');

class ModuleResolver extends Resolver {

    constructor(client) {

        super(client, {
            name: 'moduleResolver'
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    run(str) {

        const lcSearch = str.toLowerCase();
        let module;

        if(/(.{1,20}):(.{1,20})/iy.test(lcSearch)) {
            const matches = /(.{1,20}):(.{1,20})/iy.exec(lcSearch);
            module = this._grabModules(matches[1], matches[2]);
            if(!module) return false;
        }

        return module;

    }

    _grabModules(type, id) {

        const filter = m => (m.id === id || m.name === id) && m.type === type;

        const commands = this.client.registry.modules.filterArray(filter);
        return commands[0] || undefined;

    }

}

module.exports = ModuleResolver;
