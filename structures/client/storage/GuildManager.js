const KeyedStorage = require('./KeyedStorage.js');
const RethinkProvider = require('./RethinkProvider.js');

const defaultGuild = require('./defaultGuild.json');

class GuildManager {

    constructor(client) {

        Object.defineProperty(this, 'client', { value: client });

        this.storage = new KeyedStorage(client, 'guilds', RethinkProvider);

    }

    async _initialize() {

        this.storage.init();

        for(const guild of this.client.guilds.values()) {
            if(await this.storage.exists(guild.id)) continue;
            await this.createGuild(guild);
        }

    }

    async createGuild(guild) {
        const info = defaultGuild;
        info['id'] = guild.id;

        this.client.emit('debug', `Creating guild: ${guild.id}`);

        await this.storage.set(guild.id, info);
    }

}

module.exports = GuildManager;
