class KeyedStorage {

    constructor(client, name, provider) {

        Object.defineProperty(this, 'client', { value: client });

        this._storage = new provider(client, name);

        this._initialized = false;

    }

    async init() {
        if(this._initialized) return undefined;
        this._initialized = true;
        await this._storage.init();
    }

    async keys() {
        return await this._storage.keys();
    }

    async get(key) {
        if(typeof key !== 'string') return undefined;
        const data = await this._storage.get(key);
        return data || undefined;
    }

    async exists(key) {
        return Boolean(await this.get(key));
    }

    async set(key, data) {

        if(typeof data !== 'object') return undefined;
        data.id = key;

        return await this._storage.set(data);

    }

    async remove(key) {

        if(typeof key !== 'string') return undefined;
        return await this._storage.remove(key);

    }

    async update(key, data) {

        if(typeof data !== 'object') return undefined;
        return await this._storage.update(key, data);

    }

    async clear() {
        return await this._storage.clear();
    }

}

module.exports = KeyedStorage;
