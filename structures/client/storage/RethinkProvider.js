const rethinkdb = require('rethinkdbdash');

class RethinkProvider {

    constructor(client, name) {

        Object.defineProperty(this, 'client', { value: client });

        this.name = name;

    }

    async init() {

        this.r = rethinkdb({
            silent: true,
            log: (msg) => {
                this.client.logger.log('magenta', msg);
            }
        });

    }

    async keys() {

        try {
            const data = await this.r.db('Apollo').table('guilds').getField('id').run();
            return data || [];
        } catch(error) {
            this.client.emit('error', `Provider error: ${error}`);
        }

    }

    async get(key) {

        try {
            const data = await this.r.db('Apollo').table('guilds').get(key).run();
            return data || undefined;
        } catch(error) {
            this.client.emit('error', `Provider error: ${error}`);
        }

    }

    async set(data) {

        try {
            const response = await this.r.db('Apollo').table('guilds').insert(data).run();
            return response;
        } catch(error) {
            this.client.emit('error', `Provider error: ${error}`);
        }

    }

    async remove(key) {

        try {
            const response = await this.r.db('Apollo').table('guilds').get(key).delete().run();
            return response;
        } catch(error) {
            this.client.emit('error', `Provider error: ${error}`);
        }

    }

    async update(key, data) {

        try {
            const response = await this.r.db('Apollo').table('guilds').get(key).update(data).run();
            return response;
        } catch(error) {
            this.client.emit('error', `Provider error: ${error}`);
        }

    }

    async clear() {
        try {
            const response = await this.r.db('Apollo').table('guilds').delete().run();
            return response;
        } catch(error) {
            this.client.emit('error', `Provider error: ${error}`);
        }
    }

}

module.exports = RethinkProvider;
