class Utility {

    constructor(client) {

        Object.defineProperty(this, 'client', { value: client });

    }

    getModule(searchString) {
        const lcSearch = searchString.toLowerCase();
        const filter = m=>m.type === ('command' || m.type === 'inhibitor') && m.resolveable.toLowerCase().includes(lcSearch);

        const commands = this.client.commandHandler.modules.filterArray(filter);
        const inhibitors = this.client.inhibitorHandler.modules.filterArray(filter);

        const arr = [...commands, ...inhibitors];
        return arr[0] || undefined;
    }


}

module.exports = Utility;
