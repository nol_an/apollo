const path = require('path');
const { Client } = require('discord.js');

const Registry = require('./Registry.js');
const MessageHandler = require('./handlers/MessageHandler.js');
const CommandHandler = require('./handlers/CommandHandler.js');
const Logger = require('./Logger.js');
const Utility = require('./Utility.js');
const GuildManager = require('./storage/GuildManager.js');

const ApolloError = require('./ApolloError.js');

const emojis = require('../../emojis.json');

class ApolloClient extends Client {

    constructor(options = {}) {

        super();

        this.registry = new Registry(this);

        this.messageHandler = new MessageHandler(this);
        this.commandHandler = new CommandHandler(this);

        this.logger = new Logger(this);
        this.util = new Utility(this);
        this.guildManager = new GuildManager(this);

        this.prefix = options.prefix;
        this._emojis = emojis;
        this._options = options;
        this._built = false;

        this.on('message', (message) => {
            this.messageHandler.handle(message);
        });

    }

    build() {

        if(this._built) throw new ApolloError('ALREADY_BUILT');

        this.registry.load(path.join(process.cwd(), 'commands'));
        this.registry.load(path.join(process.cwd(), 'structures', 'client', 'inhibitors'));
        this.registry.load(path.join(process.cwd(), 'structures', 'client', 'resolvers'));

        this._built = true;
        return this;

    }


}

module.exports = ApolloClient;
