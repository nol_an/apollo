const { Collection } = require('discord.js');
const path = require('path');
const fs = require('fs');

const ApolloError = require('./ApolloError.js');
const Group = require('./interfaces/Group.js');
const Module = require('./interfaces/Module.js');

class Registry {

    constructor(client) {

        Object.defineProperty(this, 'client', { value: client });

        this.modules = new Collection();

    }

    load(directory, name = undefined) {

        if(!directory || typeof directory !== 'string') throw new ApolloError('INVALID_DIRECTORY', directory);
        const files = this.constructor.readdirRecursive(directory);

        const registered = [];

        for(const { filePath, file } of files) {

            if(name && file !== name) continue;

            const func = require(filePath);
            const module = new func(this.client);

            registered.push(this.register(module, filePath));

        }

        return registered;

    }

    register(module, filePath = undefined, reload = false) {

        if(!module.constructor) {
            throw new ApolloError('MODULE_NOT_CLASS', module.id || module.name);
        }

        if(!(module instanceof Module)) {
            throw new ApolloError('CLASS_NOT_MODULE', module.constructor.name);
        }

        if(this.modules.has(module.resolveable)) {
            throw new ApolloError('MODULE_REREGISTER', module.constructor.name);
        }

        if(module.group && typeof module.group === 'string') {
            const group = this.modules.has(`group:${module.group}`)
                ? this.modules.get(`group:${module.group}`)
                : this.register(new Group(this.client, { name: module.group }));
            if(module.update) module.update({ group });
            group.modules.set(module.resolveable, module);
        }

        if(!reload) this.client.emit('moduleUpdate', { type: 'register', res: module });

        if(typeof module.update === 'function' && filePath) module.update({ filePath });

        this.modules.set(module.resolveable, module);
        return module;

    }

    deregister(mod) {
        this.modules.delete(mod.resolveable);
        if(mod.group) mod.group.modules.delete(mod.resolveable);
        return true;
    }

    static readdirRecursive(directory) {

        const result = [];

        (function read(dir) {
            const files = fs.readdirSync(dir);

            for (const file of files) {
                const filePath = path.join(dir, file);

                if (fs.statSync(filePath).isDirectory()) {
                    read(filePath);
                } else {
                    result.push({ filePath, file });
                }

            }

        }(directory));

        return result;

    }



}

module.exports = Registry;
