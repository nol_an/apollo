const escapeRegex = require('escape-string-regexp');
const { Collection } = require('discord.js');
const { stripIndents } = require('common-tags');

const CommandMessage = require('../interfaces/CommandMessage');
const InhibitorHandler = require('./InhibitorHandler.js');

class CommandHandler {

    constructor(client) {

        Object.defineProperty(this, 'client', { value: client });

        this.inhibitorHandler = new InhibitorHandler(client);

        this.resolved = new Collection();
        this._commandPattern = null;

    }

    async handle(message, args) {

        const commandMessage = await this.parseCommand(message, args);
        if(!commandMessage) return undefined;

        const inhibitor = await this.inhibitorHandler.handle(commandMessage);

        if(inhibitor && inhibitor.error) {
            await commandMessage.respond(`${inhibitor.message} \`[${inhibitor.resolveable}]\``, { emoji: 'failure' });
            return undefined;
        }

        const resolved = await commandMessage._resolve();
        this.resolved.set(resolved.UUID, resolved);

        if(resolved.error) await this._resolveError(resolved);
        return resolved;

    }

    async _resolveError(resolved) {

        const messages = {
            //Command
            COMMAND_ERROR: ({ command }, UUID) => stripIndents`The module **${command.resolveable}** had issues executing. \`[${UUID}]\`
                Contact **${this.client.users.has(this.client._options.owner) ? this.client.users.get(this.client._options.owner).tag : 'the bot owner'}** about this issue. You can find support here: ${this.client._options.invite}`,

            //Flags

        };

        const message = messages[resolved.result](resolved.data, resolved.UUID);
        if(resolved.data.pendingMessage) resolved.data.pendingMessage.edit(message, { emoji: 'failure' });
        else await resolved.data.respond(message, { emoji: 'failure' });

    }


    async parseCommand(message, args) {

        if(!this._commandPattern) this._createCommandPattern();

        let commandMessage = await this._matchCommand(message, args, this._commandPattern, 2);
        if(!commandMessage && !message.guild) commandMessage = await this._matchCommand(message, args, /^([^\s]+)/i);
        return commandMessage;

    }

    _createCommandPattern() {
        const escapedPrefix = escapeRegex(this.client.prefix);

        this._commandPattern = new RegExp(`^(${escapedPrefix}\\s*|<@!?${this.client.user.id}>\\s+(?:${escapedPrefix})?)([^\\s]+)`, 'i');
        this.client.emit('debug', `Created command pattern: ${this._commandPattern}`);
        return this.commandPattern;
    }

    async _matchCommand(message, args, commandPattern, commandNameIndex = 1) {
        const matches = commandPattern.exec(message.content);
        if(!matches) return false;

        const commands = this._findCommands(matches[commandNameIndex], true);
        if(commands.length !== 1) return false;

        const index = args.indexOf(matches[0]);
        args.splice(index, 1);

        return new CommandMessage(this.client, { message, command: commands[0], args});
    }

    _findCommands(searchString, exact = false) {

        const lcSearch = searchString.toLowerCase();

        const matchedCommands = this.client.registry.modules.filter(m=>m.type === 'command').filterArray(exact
            ? commandFilterExact(lcSearch)
            : commandFilterInexact(lcSearch)
        );

        return matchedCommands;

    }

}

module.exports = CommandHandler;

const commandFilterExact = (search) => {
    return cmd => cmd.name === search ||
        cmd.resolveable === search ||
        (cmd.aliases.some(ali => `${cmd.type}:${ali}` === search)) ||
        (cmd.aliases && cmd.aliases.some(ali => ali === search));
};

const commandFilterInexact = (search) => {
    return cmd => cmd.name.includes(search) ||
        cmd.resolveable.includes(search) ||
        (cmd.aliases.some(ali => `${cmd.type}:${ali}`.includes(search))) ||
        (cmd.aliases && cmd.aliases.some(ali => ali.includes(search)));
};
