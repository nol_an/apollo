class InhibitorHandler {

    constructor(client) {

        Object.defineProperty(this, 'client', { value: client });

    }

    async handle({ message, command }) {

        const inhibitors = this.client.registry.modules.filter(m=>m.globalEnabled && m.type === 'inhibitor');
        if(!inhibitors.size) return undefined;

        const promises = [];

        for(const inhibitor of inhibitors.values()) {
            promises.push((async () => {
                let inhibited = inhibitor.run(message, command);
                if(inhibited instanceof Promise) inhibited = await inhibited;

                if(inhibited.error) return inhibited;
                return undefined;
            })());
        }

        const reasons = (await Promise.all(promises)).filter(r=>r);
        if(!reasons.length) return undefined;

        reasons.sort((a, b) => b.inhibitor.priority - a.inhibitor.priority);
        return reasons[0];

    }

}

module.exports = InhibitorHandler;
