class MessageHandler {

    constructor(client) {

        Object.defineProperty(this, 'client', { value: client });

    }

    async handle(message) {

        if(message.webhookID || message.author.bot) return undefined;
        if(message.guild && !message.member) {
            this.client.emit('debug', `attempting to fetch user: ${message.author.id} | typeof: ${typeof message.author.id}`);
            await message.guild.members.fetch(message.author.id);
        }

        const args = await this.parseArguments(message);

        return await this.client.commandHandler.handle(message, args);

    }

    async parseArguments(message) {
        return message.content.split(' '); //split on different spaces that arent surrounded by quotes and semiquotes ok
    }
}

module.exports = MessageHandler;
