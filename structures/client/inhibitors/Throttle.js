const Inhibitor = require('../interfaces/Inhibitor.js');

class Throttle extends Inhibitor {

    constructor(client) {

        super(client, {
            name: 'throttle'
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    run(msg, command) {

        const throttle = this.throttleCommand(msg, command);

        if(throttle) {
            throttle.usages++;
            if(throttle.usages > command.throttling.usages) {
                const remaining = (throttle.start + (command.throttling.duration * 1000) - Date.now()) / 1000;
                return super.fail(this, `You cannot use the moudle **${command.resolveable}** for another __${remaining.toFixed(2)} seconds__.`);
            }
        }

        return super.succeed(this);


    }

    throttleCommand(message, command) {
        if(!command.throttling) return undefined;
        if(message.author.id === this.client._options.owner) return undefined;
        let throttle = command.throttles.get(message.author.id);
        if(!throttle) {
            throttle = {
                start: Date.now(),
                usages: 0,
                timeout: this.client.setTimeout(() => {
                    command.throttles.delete(message.author.id);
                }, command.throttling.duration * 1000)
            };
            command.throttles.set(message.author.id, throttle);
        }
        return throttle;
    }

}

module.exports = Throttle;
