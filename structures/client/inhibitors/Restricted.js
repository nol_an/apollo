const Inhibitor = require('../interfaces/Inhibitor.js');

class Restricted extends Inhibitor {

    constructor(client) {

        super(client, {
            name: 'restricted',
            guarded: true
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    run(msg, command) {

        if(command.restricted && msg.author.id !== this.client._options.owner) return super.fail(this, `The module **${command.resolveable}** can only be ran by developers.`);
        else return super.succeed(this);

    }

}

module.exports = Restricted;
