const Inhibitor = require('../interfaces/Inhibitor.js');

class GuildOnly extends Inhibitor {

    constructor(client) {

        super(client, {
            name: 'guildOnly'
        });

    }

    run(msg, command) {

        if(!msg.guild && command.guildOnly) return super.fail(this, `The module **${command.resolveable}** can only be ran in guilds.`);
        else return super.succeed(this);

    }

}

module.exports = GuildOnly;
