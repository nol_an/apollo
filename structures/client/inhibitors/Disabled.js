const Inhibitor = require('../interfaces/Inhibitor.js');

class Disabled extends Inhibitor {

    constructor(client) {

        super(client, {
            name: 'disabled'
        });

    }

    run(msg, command) {

        if(!command.globalEnabled || (command.group && !command.group.globalEnabled)) {
            if(command.guarded) return super.succeed(this);
            return super.fail(this, `The module **${command.resolveable}** is currently disabled.`);
        }
        else return super.succeed(this);

    }

}

module.exports = Disabled;
