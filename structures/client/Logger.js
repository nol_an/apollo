const { Collection } = require('discord.js');
const chalk = require('chalk');
const moment = require('moment');
const path = require('path');
const fs = require('fs');

const clk = new chalk.constructor();

class Logger {

    constructor(client) {

        Object.defineProperty(this, 'client', { value: client });

        this.listeners = new Collection();
        this.initialize();

    }

    initialize() {

        this.listeners
            .set('ready', () => {
                this.checkReboot();
                this.log('green', `Logged in as... ${this.client.user.tag} (${this.client.user.id})`);
            })
            .set('moduleUpdate', (info) => {
                this.log('blue', `${clk.white(`[${info.type.toUpperCase()}]`)} Updated module: ${clk.bold(info.res.resolveable)}`);
            })
            .set('debug', (msg) => {
                if(msg && typeof msg === 'string' && (msg.toLowerCase().includes('heartbeat') || msg.includes('token'))) return;
                this.log('magenta', msg);
            })
            .set('error', (error) => {
                this.log('red', error);
            })
            .set('warn', (msg) => {
                this.log('yellow', msg);
            });

        for(const [event, listener] of this.listeners) this.client.on(event, listener);

    }

    formatDate() {
        return `[${moment().format("YYYY-MM-DD hh:mm:ssa")}]`;
    }

    log(color = 'blue', str) {
        // eslint-disable-next-line no-console
        console.log(`${clk[color](this.formatDate())} ${str}`);
        return undefined;
    }

    checkReboot() {

        const filePath = path.join(process.cwd(), 'reboot');
        if(!fs.existsSync(filePath)) return;

        fs.readFile(filePath, 'utf8', (error, contents) => {
            if(error) return this.client.emit('error', error);

            const content = contents.split(':');
            const channel = this.client.channels.get(content[0]);

            channel.fetchMessage(content[1]).then((message) => {
                message.edit(`${this.client._emojis.success} Successfully rebooted.`);
            });
            fs.unlinkSync(filePath);

        });

    }

}

module.exports = Logger;
