const Module = require('./Module.js');

class Command extends Module {

    constructor(client, info) {

        super(client, {
            id: info.name,
            type: 'command',
            guarded: info.guarded
        });

        Object.defineProperty(this, 'client', { value: client });

        //Main
        this.name = info.name;
        this.group = info.group || 'utility';
        this.aliases = info.aliases || [];

        //Descriptive
        this.description = info.description || 'A basic command.';
        this.examples = info.examples || [];

        //Functional
        this.restricted = Boolean(info.restricted);
        this.throttling = info.throttling || { usages: 5, duration: 10 };
        this.throttles = new Map();
        this.guildOnly = info.guildOnly || false;
        this.argType = info.argType || 'string';
        this.flags = info.flags || [];

        //Permissions
        this.clientPermissions = info.clientPermissions || [];
        this.userPermissions = info.userPermissions || [];

    }


    update(information) {

        for(const info of Object.entries(information)) this[info[0]] = info[1];
        return undefined;

    }

}

module.exports = Command;
