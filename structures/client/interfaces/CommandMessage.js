const { Collection } = require('discord.js');
const emojis = require('../../../emojis.json');

class CommandMessage {

    constructor(client, info) {

        Object.defineProperty(this, 'client', { value: client });

        this.message = info.message;
        this.command = info.command || undefined;
        this.args = info.args || [];

        this.data = undefined;

    }

    async respond(data, options = {}) {

        if(typeof data === 'string') {
            if(options.emoji) data = `${emojis[options.emoji]} ${data}`;
            if(options.reply) data = `<@!${this.message.author.id}> ${data}`;
        }

        const message = new CommandMessage(this.client, { message: await this.message.channel.send(data) });

        this.pendingMessage = message;
        return message;


    }

    async edit(str, options) {

        if(!this.message.editable) return undefined;
        let string = str;

        if(options.emoji && typeof options.emoji === 'string') string = `${emojis[options.emoji]} ${string}`;
        if(options.reply) string = `<@!${this.message.author.id}> ${string}`;

        this.message.edit(string);

    }

    async _resolve() {

        const UUID = (new Date()).getTime().toString(36);

        const flags = await this._resolveFlags();
        const args = await this._parseArguments();

        try {
            const command = this.command.run(this, { args, flags });
            if(command instanceof Promise) await command;
            return { error: false, data: this, result: command, UUID };
        } catch(error) {
            this.client.emit('error', `Command Error | ${this.command.resolveable} | UUID: ${UUID} :\n${error.stack ? error.stack : error}`);
            return { error: true, data: this, result: 'COMMAND_ERROR', UUID };
        }

    }

    async _parseArguments() {

        let args;

        switch(this.command.argType) {
            case 'string':
                args = this.args.join(' ').trim();
            break;
            case 'array':
                args = this.args;
            break;
        }

        return args;

    }

    async _resolveFlags() {

        const flags = new Collection();
        if(this.command.flags.length === 0) return flags;

        for(const arg of this.args) {

            const matches = /(-{1,2})([a-z]+)(?::["](.+)["]|:([0-9]+))?/g.exec(arg);
            if(!matches) continue;

            const size = matches[1].length === 1 ? 'small' : 'large';
            const possibilities = this.command.flags
                .filter(f=>f.matches[size].includes(matches[2].toLowerCase()));

            if(possibilities.length === 0) return flags;

            const query = matches[3] || matches[4] || ""; //probably do some query filtering to make sure that these are indeed numbers or strings..
            //and then return errors if not, since you know, i made this entire error system and shit. >:|

            for(const flag of possibilities) {
                flags.set(flag.name, {
                    name: flag.name,
                    match: matches.input,
                    query
                });
            }

        }

        flags.map(f=>this.args.splice(this.args.indexOf(f.match), 1));

        if(flags.some(f=>f.singular)) return flags.filter(f=>f.singular);
        return flags;

    }


    get id() {
        return this.message.id;
    }

    get createdTimestamp() {
        return this.message.createdTimestamp;
    }

    get author() {
        return this.message.author;
    }

    get channel() {
        return this.message.channel;
    }

}

module.exports = CommandMessage;
