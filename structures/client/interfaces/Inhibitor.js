const Module = require('./Module.js');

class Inhibitor extends Module {

    constructor(client, info) {

        super(client, {
            id: info.name,
            type: 'inhibitor',
            guarded: info.guarded
        });

        Object.defineProperty(this, 'client', { value: client });

        this.name = info.name;
        this.priority = info.priority || 0;

    }

    succeed(inhibitor) {
        return {
            error: false,
            inhibitor
        };
    }

    fail(inhibitor, message) {
        return {
            error: true,
            message,
            resolveable: this.resolveable
        };
    }

    update(information) {
        for(const info of Object.entries(information)) this[info[0]] = info[1];
        return undefined;
    }

}

module.exports = Inhibitor;
