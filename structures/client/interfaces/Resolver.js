const Module = require('./Module.js');

class Resolver extends Module {

    constructor(client, info) {

        super(client, {
            id: info.name,
            type: 'resolver'
        });

        Object.defineProperty(this, 'client', { value: client });

        this.name = info.name;


    }

}

module.exports = Resolver;
