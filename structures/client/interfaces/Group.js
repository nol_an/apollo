const Module = require('./Module.js');

const { Collection } = require('discord.js');

class Group extends Module {

    constructor(client, info) {

        super(client, {
            id: info.name,
            type: 'group',
            filePath: info.filePath
        });

        Object.defineProperty(this, 'client', { value: client });

        this.name = info.name;
        this.modules = new Collection();

    }

}

module.exports = Group;
