 class Module {

    constructor(client, info) {

        Object.defineProperty(this, 'client', { value: client });

        this.id = info.id;
        this.type = info.type;
        this.filePath = info.filePath || null;

        this.guarded = info.guarded || false;
        this.globalEnabled = true;

    }

    enable() {
        if(this.guarded) return { error: true, message: 'GUARDED' };
        if(this.globalEnabled) return { error: true, message: 'ALREADY_ENABLED' };

        this.globalEnabled = true;
        this.client.emit('moduleUpdate', { type: 'enable', res: this });

        return { error: false };
    }

    disable() {
        if(this.guarded) return { error: true, message: 'GUARDED' };
        if(!this.globalEnabled) return { error: true, message: 'ALREADY_DISABLED' };

        this.globalEnabled = false;
        this.client.emit('moduleUpdate', { type: 'disable', res: this });

        return { error: false };
    }

    unload() {
        if(this.guarded) return { error: true, message: 'GUARDED' };
        if(!require.cache[this.filePath] || !this.filePath) return { error: true, message: 'NO_DATA' };
        this.client.registry.deregister(this);
        delete require.cache[this.filePath];

        this.client.emit('moduleUpdate', { type: 'unload', res: this });

        return { error: false };
    }

    reload() {

        if(!require.cache[this.filePath] || !this.filePath) return { error: true, message: 'NO_DATA' };
        let cached, newModule;

        try {
            cached = require.cache[this.filePath];
            delete require.cache[this.filePath];

            newModule = require(this.filePath);
            if(typeof newModule === 'function') newModule = new newModule(this.client);

            this.client.registry.deregister(this);
            this.client.registry.register(newModule, this.filePath, true);
        } catch(error) {
            if(cached) require.cache[this.filePath] = cached;
            return { error: true, message: 'CANNOT_FIND_MODULE' };
        }

        this.client.emit('moduleUpdate', { type: 'reload', res: this });
        return { error: false };

    }

    get resolveable() {
        return `${this.type}:${this.id}`;
    }

}

module.exports = Module;
