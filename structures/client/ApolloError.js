const messages = {
    //Client
    BUILD_ONCE: 'Can only build the client once.',
    
    //Registry
    INVALID_DIRECTORY: (directory) => `Attempted to load an invalid directory: "${directory}".`,

    //Modules
    MODULE_NOT_CLASS: (module) => `Module "${module}" must be exporting a function.`,
    MODULE_REREGISTER: (module) => `Module "${module}" is already registered.`,
    CLASS_NOT_MODULE: (module) => `Class "${module}" does not parent a module.`
};

class ApolloError extends Error {

    constructor(key, ...args) {

        if(!messages[key]) throw new TypeError(`Error key "${key}" does not exist`);
        const message = typeof messages[key] === 'function'
            ? messages[key](...args)
            : message[key];

        super(message);

        this.code = key;

    }

    get name() {
        return `ApolloError [${this.code}]`;
    }

}

module.exports = ApolloError;
