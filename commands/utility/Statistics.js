const Command = require('../../structures/client/interfaces/Command.js');
const moment = require('moment');
const momentDuration = require('moment-duration-format');
const os = require('os');
const { version } = require('discord.js');

momentDuration(moment);

class Statistics extends Command {

    constructor(client) {

        super(client, {
            name: 'statistics',
            aliases: ['stats'],
            group: 'utility',
            description: 'Displays statistics about the bot.'
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    async run(msg) {

        msg.respond(this.buildStatEmbed());

    }

    buildStatEmbed() {

        return {
            "embed": {
                "author": {
                    "name": "Apollo Statistics",
                    "icon_url": this.client.user.displayAvatarURL
                },
                "description": "A short description of the bot's statistics.",
                "fields": [
                    {
                        "name": "Guilds",
                        "value": this.client.guilds.size,
                        "inline": true
                    },
                    {
                        "name": "Users",
                        "value": this.client.users.size,
                        "inline": true
                    },
                    {
                        "name": "Client Uptime",
                        "value": `${moment.duration(this.client.uptime/1000, 'seconds').format('h[h] m[m] s[s]')}`,
                        "inline": true
                    },
                    {
                        "name": "Memory Usage",
                        "value": `${(process.memoryUsage().heapUsed/1024/1024).toFixed(2)}MB`,
                        "inline": true
                    },
                    {
                        "name": "Server Uptime",
                        "value": `${moment.duration(Math.floor(os.uptime()), 'seconds').format('h[h] m[m] s[s]')}`,
                        "inline": true
                    },
                    {
                        "name": "Library",
                        "value": `discord.js v${version}`,
                        "inline": true
                    }
                ]
            }
        };

    }

}

module.exports = Statistics;
