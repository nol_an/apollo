const fs = require('fs');
const path = require('path');

const Command = require('../../structures/client/interfaces/Command.js');

class Reboot extends Command {

    constructor(client) {

        super(client, {
            name: 'reboot',
            group: 'utility',
            description: 'Checks if the bot is online or offline.',
            aliases: ['restart'],
            restricted: true,
            guarded: true
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    async run(msg) {
        await msg.respond('Rebooting...', { emoji: 'loading' });
        this.client.emit('warn', `[REBOOT]: Reboot executed by user ${msg.author.tag}.`);

        await this.createLogFile(msg);
        process.exit();
    }

    async createLogFile(msg) {
        const filePath = path.join(process.cwd(), 'reboot');
        if(fs.existsSync(filePath)) return;
        fs.writeFileSync(filePath, `${msg.channel.id}:${msg.pendingMessage.id}`, (error) => {
            if(error) return this.client.emit('error', error);
        });
    }

}

module.exports = Reboot;
