const { stripIndents } = require('common-tags');

const Command = require('../../structures/client/interfaces/Command.js');

class Help extends Command {

    constructor(client) {

        super(client, {
            name: 'help',
            group: 'utility',
            description: 'Displays help for commands.',
            flags: [
                {
                    name: 'all',
                    description: 'Shows all of the commands.',
                    matches: {
                        small: ['a'],
                        large: ['all']
                    },
                }
            ]
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    async run(msg, { args, flags }) {

        if(args) {
            const command = await this.getCommand(msg, args);
            if(!command) return undefined;

            const embed = await this.buildCommandEmbed(command);
            msg.respond(embed);

        } else {
            const all = flags.has('all') ? true : false;
            const embed = await this.buildHelpEmbed(all);
            msg.respond(embed);
        }

    }

    async getCommand(msg, args) {

        const commands = this.client.commandHandler._findCommands(args);

        if(commands.length === 0) {
            await msg.respond('Unable to find any commands with those arguments', { emoji: 'failure' });
            return undefined;
        }

        if(commands.length > 1) {
            await msg.respond(`Found multiple commands with those arguments: ${commands.map(c=>`\`${c.name}\``).join(', ')}`, { emoji: 'failure' });
            return undefined;
        }

        return commands[0];

    }

    async buildHelpEmbed(all) {

        const fields = [];
        const sorted = this.client.registry.modules.filter(m=>m.type === 'group' && m.modules && m.modules.some(m=>m.type === 'command')).sort((a, b) => b.modules.size - a.modules.size);

        for(const group of sorted.values()) {

            let field = {
                "name": group.id,
                "value": ""
            };

            for(const command of group.modules.values()) {
                if(command.restricted && !all) continue;
                field.value += `\`${command.name}\`\n`;
                field.inline = true;
            }
            fields.push(field);

        }

        return {
            "embed": {
                "description": stripIndents`To use a command, use \`${this.client.prefix}[command]\` or \`@${this.client.user.tag} [command]\`.\n
                    Use \`${this.client.prefix}help [command]\` to view a detailed description on the command.
                    Use \`${this.client.prefix}help\` to view all of the commands.\n`,
                "author": {
                    "name": "Apollo Commands",
                    "icon_url": this.client.user.displayAvatarURL
                },
                "fields": fields
            }
        };

    }

    async buildCommandEmbed(command) {

        let embed = {
            "embed": {
                "author": {
                    "name": `${command.name} (${command.group.resolveable})`,
                    "icon_url": this.client.user.displayAvatarURL
                },
                "description": `${command.description}`,
                "fields": []
            }
        };

        if(command.aliases.length > 0) embed.embed.fields.push({
            "name": "Aliases",
            "value": `${command.aliases.join(', ')}`
        });

        if(command.examples.length > 0) embed.embed.fields.push({
            "name": "Examples",
            "value": `${command.examples.map(e=>`> \`${this.client.prefix}${e}\``).join('\n')}`
        });

        if(command.flags.length > 0) embed.embed.fields.push({
            "name": "Flags",
            "value": `${command.flags.map(f=>`\`${f.name}\` > ${f.description}`).join('\n')}`
        });

        return embed;

    }

}

module.exports = Help;
