const Command = require('../../structures/client/interfaces/Command.js');

class Load extends Command {

    constructor(client) {

        super(client, {
            name: 'reload',
            group: 'utility',
            description: 'Reloads client modules.',
            restricted: true,
            guarded: true
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    async run(msg, { args }) {

        const module = await this.parseModule(args);
        if(!module) {
            await msg.respond('Unable to find a module with those arguments.', { emoji: 'failure' });
            return undefined;
        }

        const result = module.reload();
        if(result.error) {
            await msg.respond(`Unable to reload this module. \`[${result.message}]\``, { emoji: 'failure' });
            return undefined;
        }

        await msg.respond(`Successfully **reloaded** module \`${module.resolveable}\`.`, { emoji: 'success' });
        return undefined;

    }

    async parseModule(args) {

        if(!args) return undefined;
        return this.client.registry.modules.get('resolver:moduleResolver').run(args);

    }


}

module.exports = Load;
