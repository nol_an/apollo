const Command = require('../../structures/client/interfaces/Command.js');

class Ping extends Command {

    constructor(client) {

        super(client, {
            name: 'ping',
            group: 'utility',
            description: 'Checks if the bot is online or offline.',
            flags: [
                {
                    name: 'ding',
                    description: 'well.. it dings. i guess.',
                    matches: {
                        small: ['d'],
                        large: ['ding', 'dong']
                    }
                }
            ]
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    async run(msg, { flags }) {
        const mode = flags.has('ding') ? 'D' : 'P';
        await msg.respond(`${mode}inging...`, { emoji: 'loading' });
        const milliseconds = msg.pendingMessage.createdTimestamp - msg.createdTimestamp;
        const number = (milliseconds/300).toFixed(0);
        const ternary = number > 1 ? number : 1;
        msg.pendingMessage.edit(`${mode}${"o".repeat(ternary)}ng! \`${milliseconds}ms\``, { emoji: 'success' });
    }

}

module.exports = Ping;
