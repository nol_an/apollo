const Command = require('../../structures/client/interfaces/Command.js');

class Disable extends Command {

    constructor(client) {

        super(client, {
            name: 'disable',
            group: 'utility',
            description: 'Disables client modules.',
            restricted: true,
            guarded: true
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    async run(msg, { args }) {

        const module = await this.parseModule(args);
        if(!module) {
            await msg.respond('Unable to find a module with those arguments.', { emoji: 'failure' });
            return undefined;
        }

        const result = module.disable();

        if(result.error) {
            await msg.respond(`Unable to disable this module. \`[${result.message}]\``, { emoji: 'failure' });
            return undefined;
        }

        await msg.respond(`Successfully **disabled** module \`${module.resolveable}\`.`, { emoji: 'success' });
        return undefined;

    }

    async parseModule(args) {

        if(!args) return undefined;
        return this.client.registry.modules.get('resolver:moduleResolver').run(args);

    }

}

module.exports = Disable;
