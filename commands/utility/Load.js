const Command = require('../../structures/client/interfaces/Command.js');
const path = require('path');
const ApolloError = require('../../structures/client/ApolloError.js');

class Load extends Command {

    constructor(client) {

        super(client, {
            name: 'load',
            group: 'utility',
            description: 'Loads client modules.',
            argType: 'array',
            flags: [
                {
                    name: 'path',
                    description: 'Filters loading modules based on file paths.',
                    matches: {
                        small: ['p'],
                        large: ['path', 'filepath']
                    },
                    argType: 'string'
                }
            ],
            restricted: true,
            guarded: true
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    async run(msg, { args, flags }) {

        const directory = flags.has('path')
            ? path.join(process.cwd(), flags.get('path').query)
            : path.join(process.cwd(), 'commands');

        const fileName = args[0];

        try {

            const result = this.client.registry.load(directory, fileName);
            if(result.length === 0){
                await msg.respond('Failed to load any modules.', { emoji: 'failure' });
                return undefined;
            }

            await msg.respond(`Successfully **loaded** module${result.length > 1 ? 's' : ''} \`${result.map(m=>m.resolveable).join(', ')}\`.`, { emoji: 'success' });

        } catch(error) {
            const code = error instanceof ApolloError ? error.code : this.client.emit('error', `Error loading a module: ${error}`);
            await msg.respond(`Failed to load that module due to an error. \`[${code}]\``, { emoji: 'failure' });
            return undefined;
        }

    }


}

module.exports = Load;
