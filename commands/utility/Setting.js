const Command = require('../../structures/client/interfaces/Command.js');

class Setting extends Command {

    constructor(client) {

        super(client, {
            name: 'setting',
            aliases: ['settings', 'config', 'configuration'],
            group: 'utility',
            description: 'Sets settings for certain permissions.',
            restricted: true
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    async run(msg, { args }) {

        const setting = await this.parseSetting(args);
        if(!setting) return msg.respond('Invalid setting name!', { emoji: 'failure' });

    }

    async parseSetting() {



    }




}

module.exports = Setting;
