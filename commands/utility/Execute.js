const childProcess = require('child_process');

const Command = require('../../structures/client/interfaces/Command.js');
const emojis = require('../../emojis.json');

class Execute extends Command {

    constructor(client) {

        super(client, {
            name: 'execute',
            group: 'utility',
            description: 'Executes bash commands.',
            aliases: [
                'exec'
            ],
            restricted: true,
            flags: [
                {
                    name: 'log',
                    description: 'Logs the result in the console.',
                    matches: {
                        small: ['l'],
                        large: ['log']
                    }
                },
                {
                    name: 'log',
                    description: 'Hides the result from the text-channel.',
                    matches: {
                        small: ['h'],
                        large: ['hide', 'hidden']
                    }
                }
            ]
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    async run(msg, { args, flags }) {

        let embed = {};

        childProcess.exec(args, {}, (error, stdout, stderror) => {
            if(error) {
                embed.title = `${emojis.failure} ERROR`;
                embed.color = 0xff7575;
                embed.description = `\`\`\`js\n${stderror}\`\`\``;

                msg.channel.send({embed});
                return undefined;
            }

            if(flags.has('log')) this.client.emit('debug', `Execute Result: ${stdout}`);
            if(stdout.length > 2000) stdout = `${stdout.substring(0, 2000)}...`;

            embed.title = `${emojis.success} SUCCESS`;
            embed.color = 0x43d490;
            embed.description = flags.has('hide') ? '```js\n<hidden>```' : `\`\`\`js\n${stdout}\`\`\``;

            msg.channel.send({embed});
            return undefined;

        });

    }

}

module.exports = Execute;
