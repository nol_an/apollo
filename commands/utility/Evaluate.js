const util = require('util');
const username = require('os').userInfo().username;

const emojis = require('../../emojis.json');
const Command = require('../../structures/client/interfaces/Command.js');

class Evaluate extends Command {

    constructor(client) {

        super(client, {
            name: 'evaluate',
            group: 'utility',
            aliases: ['eval'],
            description: 'Runs arbitrary javascript.',
            restricted: true,
            guarded: true,
            flags: [
                {
                    name: 'log',
                    description: 'Logs the result in the console.',
                    matches: {
                        small: ['l'],
                        large: ['log']
                    }
                },
                {
                    name: 'hide',
                    description: 'Hides the result from the text-channel.',
                    matches: {
                        small: ['h'],
                        large: ['hide', 'hidden']
                    }
                }
            ]
        });

        Object.defineProperty(this, 'client', { value: client });

    }


    async run(msg, { args, flags }) {

        let embed = {};

        try {

            let evaled = eval(args);
            if(evaled instanceof Promise) await evaled;

            if(typeof evaled !== 'string') evaled = util.inspect(evaled);

            evaled = evaled
                .replace(new RegExp(this.client.token, 'g'), '<redacted>')
                .replace(new RegExp(username, 'g'), '<redacted>');

            if(flags.has('log')) this.client.emit('debug', `Evaluate Result: ${evaled}`);
            if(evaled.length > 2000) evaled = `${evaled.substring(0, 2000)}...`;

            embed.title = `${emojis.success} SUCCESS`;
            embed.color = 0x43d490;
            embed.description = flags.has('hide') ? '```js\n<hidden>```' : `\`\`\`js\n${evaled}\`\`\``;

        } catch(error) {

            embed.title = `${emojis.failure} ERROR`;
            embed.color = 0xff7575;
            embed.description = `\`\`\`js\n${error}\`\`\``;

        }

        msg.respond({embed});

    }

}

module.exports = Evaluate;
