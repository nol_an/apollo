const Command = require('../../structures/client/interfaces/Command.js');

class Meme extends Command {

    constructor(client) {

        super(client, {
            name: 'meme',
            group: 'utility',
            description: 'Checks if the bot is online or offline.'
        });

        Object.defineProperty(this, 'client', { value: client });

    }

    async run(msg, { emojis }) {
        this.statusMsg = await msg.channel.send(`${emojis.loading} Pinging...`);
        const milliseconds = this.statusMsg.createdTimestamp - msg.createdTimestamp;
        const number = (milliseconds/300).toFixed(0);
        const ternary = number > 1 ? number : 1;
        this.statusMsg.edit(`${emojis.success} P${"o".repeat(ternary)}ng! \`${milliseconds}ms\``);
    }

}

module.exports = Meme;
