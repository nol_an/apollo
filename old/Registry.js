const requireAll = require('require-all');
const { Collection } = require('discord.js');
const path = require('path');

const Group = require('./interfaces/Group.js');

class Registry {

    constructor(client) {

        Object.defineProperty(this, 'client', { value: client });

        this.commands = new Collection();
        this.groups = new Collection();
        this.inhibitors = new Collection();

        this.commandsPath = null;
        this.inhibitorsPath = null;

    }

    async registerInhibitorsIn(directory) {

        if(!this.inhibitorsPath) this.inhibitorsPath = directory;

        const objs = requireAll(directory);
        const inhibitors = [];

        for(const inhibitor of Object.entries(objs)) {
            inhibitors.push({
                inhibitor: inhibitor[1],
                inhibitorPath: path.join(directory, `${inhibitor[0]}.js`)
            });
        }

        return await this.registerInhibitors(inhibitors);

    }

    async registerInhibitors(inhibitors) {

        for(let inhib of inhibitors) {

            let { inhibitor, inhibitorPath } = inhib;

            if(typeof inhibitor === 'function') inhibitor = new inhibitor(this.client);
            inhibitor.update({
                filePath: inhibitorPath
            });

            if(this.inhibitors.some(i=>i.name === inhibitor.name)) {
                this.client.emit('error', `Attempted to reregister inhibitor: ${inhibitor.resolveable}`);
                continue;
            }

            this.client.emit('register', {
                type: 'inhibitor',
                res: inhibitor
            });

            this.inhibitors.set(inhibitor.name, inhibitor);

        }

    }

    async registerCommandsIn(directory) {

        if(!this.commandsPath) this.commandsPath = directory;

        const groups = requireAll(directory);
        const commands = [];

        for(const group of Object.entries(groups)) {
            const groupName = group[0];
            for(const command of Object.entries(group[1])) {
                commands.push({
                    command: command[1],
                    group: groupName,
                    groupPath: path.join(directory, group[0]),
                    commandPath: path.join(directory, group[0], `${command[0]}.js`)
                });
            }
        }

        return await this.registerCommands(commands);

    }

    async registerCommands(commands) {

        for(let cmd of commands) {

            let { command, group, groupPath, commandPath } = cmd;

            if(!this.groups.has(group)) await this.registerGroup(group, groupPath);
            group = this.groups.get(group);

            if(typeof command === 'function') command = new command(this.client);
            command.update({
                group,
                filePath: commandPath
            });

            if(this.commands.some(c=>c.name === command.name)) {
                this.client.emit('error', `Attempted to reregister command: ${command.resolveable}`);
                continue;
            }

            group.modules.set(command.name, command);
            this.commands.set(command.name, command);

            this.client.emit('register', {
                type: 'command',
                res: command
            });

        }

        return this.commands;

    }

    async registerGroup(group, groupPath) {

        group = new Group(this.client, { name: group, filePath: groupPath });

        if(this.groups.has(group.name)) this.client.emit('error', `Attempted to reregister group: ${group.resolveable}`); //throw new ApolloError('GROUP_REREGISTER', group.name);

        this.groups.set(group.name, group);

        this.client.emit('register', {
            type: 'group',
            res: group
        });

        return group;

    }

    findCommands(searchString, exact = false) {

        const lcSearch = searchString.toLowerCase();

        const matchedCommands = this.commands.filterArray(exact ?
            commandFilterExact(lcSearch) :
            commandFilterInexact(lcSearch)
        );

        return matchedCommands;

    }

    getModule(searchString) {

        const lcSearch = searchString.toLowerCase();

        const commands = this.commands.filterArray(i=>i.resolveable.toLowerCase().includes(lcSearch));
        const inhibitors = this.inhibitors.filterArray(i=>i.resolveable.toLowerCase().includes(lcSearch));
        const groups = this.groups.filterArray(i=>i.resolveable.toLowerCase().includes(lcSearch));

        const search = commands.concat(inhibitors, groups);
        return search.length > 0 ? search[0] : null;

    }

    unloadModule(module) {

        switch(module.type) {
            case 'command':
                this.commands.delete(module.id);
                module.group.commands.delete(module.id);
                break;
            case 'inhibitor':
                this.inhibitors.delete(module.id);
                break;
        }

        return { error: false };

    }

}

module.exports = Registry;

const commandFilterExact = (search) => {
    return cmd => cmd.name === search ||
        cmd.resolveable === search ||
        (cmd.aliases.some(ali => `${cmd.type}:${ali}` === search)) ||
        (cmd.aliases && cmd.aliases.some(ali => ali === search));
};

const commandFilterInexact = (search) => {
    return cmd => cmd.name.includes(search) ||
        cmd.resolveable.includes(search) ||
        (cmd.aliases.some(ali => `${cmd.type}:${ali}`.includes(search))) ||
        (cmd.aliases && cmd.aliases.some(ali => ali.includes(search)));
};
