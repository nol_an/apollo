const Setting = require('../interfaces/Setting.js');

class Playlists extends Setting {

    constructor(client) {

        super(client, {
            name: 'playlists',
            group: 'music',
            aliases: ['playlist']
        });

    }

}

module.exports = Playlists;
