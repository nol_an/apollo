const rethinkdb = require('rethinkdbdash');

class Provider {

    constructor(client) {

        Object.defineProperty(this, 'client', { value: client });

        this.r = rethinkdb({
            silent: true,
            log: (msg) => {
                return this.client.logger.log('magenta', msg);
            }
        });

    }

    async _initialize() { //only used for vps
        await this.r.dbCreate('Apollo');
        await this.r.db('Apollo').tableCreate('guildSettings');
    }


}

module.exports = Provider;
