const { stripIndents } = require('common-tags');
const { Collection } = require('discord.js');

const emojis = require('../../../emojis.json');

class CommandHandler {

    constructor(client) {

        Object.defineProperty(this, 'client', { value: client });

    }

    async handle(message, command, args) {

        const result = await this.runInhibitors(message, command);

        //inhibitor handling
        if(result.error) {
            await message.channel.send(`${emojis.failure} ${result.message} \`[${result.inhibitor.resolveable}]\``);
            this.client.emit('commandBlocked', `Blocked Command: ${command.resolveable}`);
            return undefined;
        }

        const info = await this.parseArguments(command, args);
        if(!info) return undefined;

        //parameter error handling
        if(info.error) {
            await message.channel.send(`${emojis.failure} ${info.errorMessage}`);
            return undefined;
        }

        try {
            const promise = command.run(message, { args:info.data.arguments, parameters:info.data.parameters, emojis });
            const returnValue = await promise;
            return returnValue;
        } catch(error) {
            const UUID = (new Date()).getTime().toString(36);
            const owner = this.client.users.get(this.client._options.owner);

            this.client.emit('error', `Command Error | ${command.resolveable} | UUID: ${UUID} :\n${error.stack ? error.stack : error}`);

            const msg = stripIndents`${emojis.failure} There was an error executing this command. \`[${UUID}]\`
            Contact **${owner ? owner.tag : 'the bot owner'}** about this error. Find support here: ${this.client._options.invite}`;

            if(command.statusMsg) await command.statusMsg.edit(msg);
            else await message.channel.send(msg);

            return undefined;
        }

    }

    async parseArguments(command, args) {

        args = args.split(' ');
        const matched = [];

        for(const arg of args) {

            const matches = /(-{1,2})([a-z]+)(?::["'](.+)["']|:([0-9]+))?/g.exec(arg);
            if(!matches || !matches[1]) continue;

            const size = matches[1].length === 1 ? 'small' : 'large';

            if(!command.parameters) break;
            for(const param of Object.entries(command.parameters)) {
                if(!param[1].matches[size]) continue;
                for(const match of param[1].matches[size]) {
                    if(match === matches[2]) matched.push({
                        parameter: param[1],
                        parameterName: param[0],
                        matches
                    });
                    if(param[1].singular) break;
                    else continue;
                }

            }

        }

        const parameters = new Collection();

        for(const match of matched) {
            let info = {
                parameter: match.parameter,
                parameterName: match.parameterName,
                matches: match.matches,
                input: match.matches.input
            };

            switch(match.parameter.argType) {
                case 'string':
                    if(!match.matches[3] && match.parameter.arguments && !match.parameter.default) return { error: true, errorMessage: `The parameter \`${match.parameterName}\` requires a **${match.parameter.argType}** argument.` };
                    info.query = match.matches[3] || match.parameter.default;
                    break;
                case 'number':
                    if(!match.matches[4] && match.parameter.arguments && !match.parameter.default) return { error: true, errorMessage: `The parameter \`${match.parameterName}\` requires a **${match.parameter.argType}** argument.` };
                    info.query = parseInt(match.matches[4]) || match.parameter.default;
                    if(isNaN(info.query)) return { error: true, errorMessage: `The parameter \`${match.parameterName}\` requires a **${match.parameter.argType}** argument.`};
                    break;
            }

            const index = args.indexOf(info.input);
            args.splice(index, 1);

            parameters.set(info.parameterName, info);

        }

        switch(command.argType) {
            case 'string':
                args = args.join(' ');
                break;
            case 'array':
                break;
        }

        return { error: false, data: { arguments: args, parameters } };

    }

    async runInhibitors(message, command) {

        const inhibited = await this.client.inhibitorHandler.handle(message, command);
        if(!inhibited) return { error: false };

        return inhibited;

    }

    findCommands(searchString, exact = false) {

        const lcSearch = searchString.toLowerCase();

        const matchedCommands = this.client.registry.modules.filter(m=>m.type === 'command').filterArray(exact
            ? commandFilterExact(lcSearch)
            : commandFilterInexact(lcSearch)
        );

        return matchedCommands;

    }

}

module.exports = CommandHandler;

const commandFilterExact = (search) => {
    return cmd => cmd.name === search ||
        cmd.resolveable === search ||
        (cmd.aliases.some(ali => `${cmd.type}:${ali}` === search)) ||
        (cmd.aliases && cmd.aliases.some(ali => ali === search));
};

const commandFilterInexact = (search) => {
    return cmd => cmd.name.includes(search) ||
        cmd.resolveable.includes(search) ||
        (cmd.aliases.some(ali => `${cmd.type}:${ali}`.includes(search))) ||
        (cmd.aliases && cmd.aliases.some(ali => ali.includes(search)));
};
