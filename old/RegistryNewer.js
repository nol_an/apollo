const { Collection } = require('discord.js');
const requireAll = require('require-all');
const path = require('path');

const Group = require('./interfaces/Group.js');

class Registry {

    constructor(client) {

        Object.defineProperty(this, 'client', { value: client });

        this.modules = new Collection();
        this.groups = new Collection();

    }

    registerModulesIn(directory) {

        const objs = Object.entries(requireAll(directory));
        const groups = objs.some(o => typeof o[1] !== 'function');

        const modules = [];

        for(const obj of objs) {
            if(!groups) {
                modules.push({
                    module: obj[1],
                    modulePath: path.join(directory, `${obj[0]}.js`)
                });
            } else {
                for(const mod of Object.entries(obj[1])) {
                    modules.push({
                        module: mod[1],
                        modulePath: path.join(directory, obj[0], `${mod[0]}.js`)
                    });
                }
            }
        }

        this.registerModules(modules);

    }

    registerModules(modules) {

        for(let mod of modules) {

            let { module, modulePath } = mod;

            if(typeof module === 'function') module = new module(this.client);

            if(module.group && typeof module.group === 'string') {
                const group = this.groups.has(module.group)
                    ? this.groups.get(module.group)
                    : this.registerGroup(module.group);
                module.update({ group });
            }

            module.update({ filePath: modulePath });

            if(this.modules.filter(m=>m.type === module.type).some(m=>m.name === module.name)) {
                this.client.emit('error', `Attempted to reregister module: ${module.resolveable}`);
                continue;
            }

            this.client.emit('moduleRegister', module);

            this.modules.set(module.name, module);

        }

    }

    registerGroup(name) {

        const group = new Group(this.client, { name });

        if(this.groups.has(group.name)) this.client.emit('error', `Attempted to reregister group: ${group.resolveable}`);

        this.groups.set(group.name, group);
        this.client.emit('moduleRegister', group);

        return group;

    }

    findCommands(searchString, exact = false) {

        const lcSearch = searchString.toLowerCase();

        const matchedCommands = this.modules.filter(m=>m.type === 'command').filterArray(exact
            ? commandFilterExact(lcSearch)
            : commandFilterInexact(lcSearch)
        );

        return matchedCommands;

    }

    getModule(searchString) {

        const lcSearch = searchString.toLowerCase();

        const modules = this.modules.filterArray(m=>m.resolveable.toLowerCase().includes(lcSearch));
        return modules.length > 0 ? modules[0] : null;

    }

}

module.exports = Registry;

const commandFilterExact = (search) => {
    return cmd => cmd.name === search ||
        cmd.resolveable === search ||
        (cmd.aliases.some(ali => `${cmd.type}:${ali}` === search)) ||
        (cmd.aliases && cmd.aliases.some(ali => ali === search));
};

const commandFilterInexact = (search) => {
    return cmd => cmd.name.includes(search) ||
        cmd.resolveable.includes(search) ||
        (cmd.aliases.some(ali => `${cmd.type}:${ali}`.includes(search))) ||
        (cmd.aliases && cmd.aliases.some(ali => ali.includes(search)));
};
