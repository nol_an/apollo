const Module = require('./Module.js');

class Setting extends Module {

    constructor(client, info) {

        super(client, {
            name: info.name,
            type: 'setting',
            filePath: info.filePath
        });

        Object.defineProperty(this, 'client', { value: client });

        //Main
        this.name = info.name;
        this.group = info.group || 'utility';
        this.aliases = info.aliases || [];

        //Descriptive
        this.description = info.description || 'A basic setting.';

        //Functional
        this.group = undefined;
        this.argType = info.argType || 'boolean';

    }

    update(information) {

        for(const info of Object.entries(information)) this[info[0]] = info[1];
        return undefined;

    }

    async parseArguments() {

        //eslint-disable-next-line
        let args = undefined;

        //switch or do an object idk whatever you're feeling manerino.

    }

}

module.exports = Setting;
