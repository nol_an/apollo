const Client = require('./structures/client/Client.js');

const options = require('./options.json');

const client = new Client(options).build();

client.login(options.token);

// eslint-disable-next-line no-console
process.on('unhandledRejection', err => console.error(`Uncaught Promise Error: \n${err.stack}`));
